const path = require("path");
const fs = require("fs");
const logPath = path.join(__dirname, "../", "log.log");

const errorHandler = function (err, req, res, next) {
    console.error(err);
    const status = err.status || 500;
    const message = err.message || "Internal Server Error";
    const obj = {
        error: message
    }
    res.status(status).json(obj).end();
};

const logHandlerMiddleware = function (req, res, next) {
    const date = `${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`;
    const options = {
        timeZone: 'Asia/Kolkata',
        hour12: true,
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    };
    const time = (new Date()).toLocaleString('en-US', options);
    // const time = `${new Date().getHours()}/${new Date().getMinutes()+1}/${new Date().getSeconds()}`;

    const content = JSON.stringify({
        id: req.id,
        url: req.url,
        date: date,
        time: time
    });
    fs.appendFile(logPath, content + ",\n", (err, data) => {
        if (err) {
            console.error(err);
        }
    })
    next();
};


const logMiddleware = function (req, res, next) {
    res.status(200).sendFile(logPath);
}



module.exports = {
    errorHandler,
    logHandlerMiddleware,
    logMiddleware
}