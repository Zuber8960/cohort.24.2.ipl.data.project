// ================================ problem 5 ==============================



// Find the number of times each team won the toss and also won the match.




//===========================================================================

const path = require('path');

const csvtojson = require('csvtojson');

const matches_csv_file = path.join(__dirname, "../data/matches.csv");


const won_the_toss_won_the_match = function (req, res, next) {

    csvtojson()
        .fromFile(matches_csv_file)
        .then(jsonObj => {
            const wonTossTeam = jsonObj.filter(match => {
                return match.toss_winner == match.winner
            })

            const result = wonTossTeam.reduce((obj, match) => {
                if (obj[match.winner] == undefined) {
                    obj[match.winner] = 1;
                } else {
                    obj[match.winner]++;
                }
                return obj;
            }, {});
            res.status(200).json(result);
            // console.log(jsonObj);
        })
        .catch(error => {
            console.error(error);
            next({
                status: 500,
                message: error.message
            });
        });
};

module.exports = won_the_toss_won_the_match;

