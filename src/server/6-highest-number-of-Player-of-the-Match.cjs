// ================================ problem 6 ============================================



// Find a player who has won the highest number of Player of the Match awards for each season




//=========================================================================================

const path = require('path');

const csvtojson = require('csvtojson');

const matches_csv_file = path.join(__dirname, "../data/matches.csv");

const highest_number_of_Player_of_the_Match = function (req, res, next) {

    csvtojson()
        .fromFile(matches_csv_file)
        .then(jsonObj => {
            const playerOfMathcOfYear = jsonObj.reduce((obj, match) => {
                if (obj[match.season] == undefined) {
                    obj[match.season] = {};
                    obj[match.season][match.player_of_match] = 1;
                } else {
                    if (!obj[match.season][match.player_of_match]) {
                        obj[match.season][match.player_of_match] = 1;
                    } else {
                        obj[match.season][match.player_of_match]++;
                    }
                }
                return obj;
            }, {});

            // console.log(playerOfMathcOfYear);

            const keys = Object.keys(playerOfMathcOfYear);
            const arr = keys.map(key => {
                const values = Object.entries(playerOfMathcOfYear[key]);
                values.sort((value1, value2) => {
                    return value2[1] - value1[1];
                });
                return [key, [values[0][0], values[0][1]]];
            });
            const result = Object.fromEntries(arr);
            res.status(200).json(result).end();
        })
        .catch(error => {
            console.error(error);
            next({
                status: 500,
                message: error.message
            });
        });
};


module.exports = highest_number_of_Player_of_the_Match;