// ================================ problem 2 ========================



// Extra runs conceded per team in the year 2016



//====================================================================

const path = require('path');

const csvtojson = require('csvtojson');


const matches_csv_file = path.join(__dirname, "../data/matches.csv");
const deliveries_csv_file = path.join(__dirname, "../data/deliveries.csv");


const extra_runs_conceded_per_team_in_the_year_2016 = function (req, res, next) {

    csvtojson()
        .fromFile(matches_csv_file)
        .then(jsonObj => {
            const matchsId = jsonObj.filter(match => {
                return match.season == 2016;
            })
                .map(match => {
                    return match.id;
                })
            // console.log(matchsId);

            csvtojson()
                .fromFile(deliveries_csv_file)
                .then(deliveries => {

                    const result = deliveries.reduce((obj, delivery) => {
                        if (matchsId.includes(delivery.match_id)) {
                            if (obj[delivery.bowling_team] == undefined) {
                                obj[delivery.bowling_team] = Number(delivery.extra_runs);
                            } else {
                                obj[delivery.bowling_team] += Number(delivery.extra_runs);
                            }
                        }
                        return obj;
                    }, {});
                    res.status(200).json(result);

                })
                .catch(error => {
                    console.log(error);
                    next({
                        status: 500,
                        message: error.message
                    });
                });
        })
        .catch(error => {
            console.log(error);
            next({
                status: 500,
                message: error.message
            });
        });
};


module.exports = extra_runs_conceded_per_team_in_the_year_2016;