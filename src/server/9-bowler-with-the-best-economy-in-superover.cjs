// ================================ problem 8 ==============================



// Find the bowler with the best economy in super overs.




//===========================================================================

const path = require('path');

const csvtojson = require('csvtojson');

const deliveries_csv_file = path.join(__dirname, "../data/deliveries.csv");

// Find the bowler with the best economy in super overs


const bowler_with_best_economy_in_super_overs = function (req, res, next) {

    csvtojson()
        .fromFile(deliveries_csv_file)
        .then(jsonObj => {
            // console.log(jsonObj);
            const is_super_over_deliveres = jsonObj.filter(delivery => {
                return delivery.is_super_over != 0;
            })
                .map(delivery => {
                    return {
                        match_id: delivery.match_id,
                        bowler: delivery.bowler,
                        total_runs: delivery.total_runs,
                        over: delivery.over,
                    }
                })
            // console.log(is_super_over_deliveres);

            const bowler = is_super_over_deliveres.reduce((obj, delivery) => {
                if (obj[delivery.bowler] == undefined) {
                    obj[delivery.bowler] = {};
                    obj[delivery.bowler][delivery.match_id] = Number(delivery.total_runs);
                } else {
                    if (obj[delivery.bowler][delivery.match_id] == undefined) {
                        obj[delivery.bowler][delivery.match_id] = Number(delivery.total_runs);
                    } else {
                        obj[delivery.bowler][delivery.match_id] += Number(delivery.total_runs);
                    }
                };
                return obj;
            }, {});

            // console.log(bowler);
            const keys = Object.keys(bowler);
            const economicalBowler = keys.reduce((arr, key) => {
                const bowlerDetails = Object.entries(bowler[key]);
                bowlerDetails.sort((strike1, strike2) => {
                    return strike1[1] - strike2[1];
                });
                if (arr[1] > bowlerDetails[0][1]) {
                    arr[1] = bowlerDetails[0][1];
                    arr[0] = key;
                }
                return arr;
            }, ["", Infinity]);

            const topEconomicalBowler = Object.fromEntries([economicalBowler]);
            // console.log(topEconomicalBowler);
            res.status(200).json(topEconomicalBowler).end();
        })
        .catch(error => {
            console.error(error);
            next({
                status: 500,
                message: error.message
            });
        });
};


module.exports = bowler_with_best_economy_in_super_overs;


