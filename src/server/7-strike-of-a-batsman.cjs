// ================================ problem 7 ==============================



// Find the strike rate of a batsman for each season.




//===========================================================================

const csvtojson = require('csvtojson');
const path = require('path');

const matches_csv_file = path.join(__dirname, "../data/matches.csv");
const deliveries_csv_file = path.join(__dirname, "../data/deliveries.csv");

// Find the strike rate of a batsman for each season


const find_the_strike_rate_of_a_batsman = function (req, res, next) {

    csvtojson()
        .fromFile(matches_csv_file)
        .then(jsonObj => {
            const seasons = jsonObj.reduce((obj, match) => {
                if (obj[match.season] == undefined) {
                    obj[match.season] = [match.id];
                } else {
                    obj[match.season].push(match.id);
                }
                return obj;
            }, {});

            // console.log(seasons);
            csvtojson()
                .fromFile(deliveries_csv_file)
                .then(jsonObj => {
                    jsonObj = jsonObj.filter(delivery => {
                        return (delivery.wide_runs == '0');
                    });

                    // console.log(jsonObj);

                    function matchesStrikeRate(batsman, ids) {
                        const result = jsonObj.reduce((obj, delivery) => {
                            if (batsman.includes(delivery.batsman) && ids.includes(delivery.match_id)) {
                                const runs = delivery.batsman_runs ? Number(delivery.batsman_runs) : 0;
                                if (obj[delivery.batsman] == undefined) {
                                    obj[delivery.batsman] = delivery.batsman;
                                    obj[delivery.batsman] = {};
                                    obj[delivery.batsman].runs = runs;
                                    obj[delivery.batsman].balls = 1;
                                } else {
                                    obj[delivery.batsman].runs += runs;
                                    obj[delivery.batsman].balls++;
                                }
                            }
                            return obj;
                        }, {});

                        const keys = Object.keys(result);
                        const ans = keys.reduce((obj, key) => {
                            const { runs, balls } = result[key];
                            const strike_rate = (runs * 100 / balls).toFixed(2);
                            obj[key] = strike_rate;
                            return obj;
                        }, {});
                        return ans;
                    };
                    // console.log(matchesStrikeRate(["SC Ganguly","BB McCullum"], [
                    //     '308', '309', '310', '311', '312', '313', '314',
                    //     '315', '316', '317', '318', '319', '320', '321',
                    //     '322', '323', '324', '325', '326', '327', '328',
                    //     '329', '330', '331', '332', '333', '334', '335',
                    //     '336', '337', '338', '339', '340', '341', '342',
                    //     '343', '344', '345', '346', '347', '348', '349',
                    //     '350', '351', '352', '353', '354', '355', '356',
                    //     '357', '358', '359', '360', '361', '362', '363',
                    //     '364', '365', '366', '367', '368', '369', '370',
                    //     '371', '372', '373', '374', '375', '376', '377',
                    //     '378', '379', '380', '381'
                    //   ]))

                    const allBatsmansArray = jsonObj.reduce((array, delivery) => {
                        if (!array.includes(delivery.batsman)) {
                            array.push(delivery.batsman);
                        }
                        return array;
                    }, []);
                    // console.log(allBatsmansArray);


                    const years = Object.keys(seasons);

                    const output = years.reduce((newObj, year) => { 
                        newObj[year] = matchesStrikeRate(allBatsmansArray, seasons[year]);
                        return newObj;
                    }, {});
                    // console.log(output);
                    //output == this object key is years and values is batsman and his strike rate;
                    const newOutput = allBatsmansArray.reduce((obj,batman) => {
                        obj[batman] = {};
                        const innerOutput = years.reduce((innerObj,year) => {
                            if(output[year][batman]){
                                innerObj[year] = output[year][batman];
                            }
                            return innerObj;
                        },{});
                        obj[batman] = innerOutput;
                        return obj;
                    },{});
                    // console.log(newOutput);
                    //newOutput = this object key is batsman name and values is year and batsman strike rate;
                    res.status(200).json(newOutput).end();
                })
                .catch(error => {
                    console.error(error);
                    next({
                        status: 500,
                        message: error.message
                    });
                });
        })
        .catch(error => {
            console.error(error);
            next({
                status: 500,
                message: error.message
            });
        });
};

module.exports = find_the_strike_rate_of_a_batsman;