// ================================ problem 4 ========================



// Top 10 economical bowlers in the year 2015



//====================================================================


const csvtojson = require('csvtojson');

const path = require('path');


const matches_csv_file = path.join(__dirname, "../data/matches.csv");
const deliveries_csv_file = path.join(__dirname, "../data/deliveries.csv");

const top_10_economical_bowlers_in_the_year_2015 = function (req, res, next) {

    csvtojson()
        .fromFile(matches_csv_file)
        .then(jsonObj => {
            // console.log(jsonObj);
            const matchsId = jsonObj.filter(match => {
                return match.season == 2015;
            })
                .map(match => {
                    return match.id;
                });
            // console.log(matchsId);

            csvtojson()
                .fromFile(deliveries_csv_file)
                .then(jsonObj => {
                    // console.log(jsonObj);
                    const result = jsonObj.reduce((obj, delivery) => {
                        if (matchsId.includes(delivery.match_id)) {
                            if (delivery.wide_runs == "0" && delivery.noball_runs == "0") {
                                if (obj[delivery.bowler] == undefined) {
                                    obj[delivery.bowler] = {
                                        runs: Number(delivery.total_runs),
                                        ball: 1,
                                    }
                                } else {
                                    obj[delivery.bowler].runs += Number(delivery.total_runs);
                                    obj[delivery.bowler].ball++;
                                }
                            } else {
                                if (obj[delivery.bowler] == undefined) {
                                    obj[delivery.bowler] = {
                                        runs: Number(delivery.total_runs),
                                        ball: 0
                                    };
                                } else {
                                    obj[delivery.bowler].runs += Number(delivery.total_runs);
                                }
                            }
                        }
                        return obj;
                    }, {});

                    // console.log(result);
                    const keys = Object.keys(result);
                    const arrayOfBowler = keys.map(key => {
                        const { runs } = result[key];
                        const { ball } = result[key];
                        const economy = ((runs / ball) * 6).toFixed(2);
                        return [key, economy];
                    });

                    let topEconomicalBowler = arrayOfBowler.sort((player1, player2) => {
                        return player1[1] - player2[1];
                    })

                    topEconomicalBowler = topEconomicalBowler.slice(0, 10);
                    topEconomicalBowler = Object.fromEntries(topEconomicalBowler);
                    // console.log(topEconomicalBowler);

                    // fs.writeFileSync(filePath, JSON.stringify(topEconomicalBowler, null, 2), "utf-8");
                    res.status(200).json(topEconomicalBowler);

                })
        })
        .catch(error => {
            console.log(error);
            next({
                status: 500,
                message: error.message
            });
        });
};


module.exports = top_10_economical_bowlers_in_the_year_2015;