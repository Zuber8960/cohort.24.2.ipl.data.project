// ================================ problem 8 ==============================



// Find the highest number of times one player has been dismissed by another player




//===========================================================================

const path = require('path');

const csvtojson = require('csvtojson');

const deliveries_csv_file = path.join(__dirname, "../data/deliveries.csv");

const one_player_dismissed_by_another_player = function (req, res, next) {

    csvtojson()
        .fromFile(deliveries_csv_file)
        .then(jsonObj => {
            const dismissalList = jsonObj.filter(delivery => {
                return (delivery.player_dismissed != "" && delivery.dismissal_kind != "run out");
            })
            const result = dismissalList.reduce((obj, delivery) => {
                if (obj[delivery.player_dismissed]) {
                    if (obj[delivery.player_dismissed][delivery.bowler]) {
                        obj[delivery.player_dismissed][delivery.bowler]++;
                    } else {
                        obj[delivery.player_dismissed][delivery.bowler] = 1;
                    }
                } else {
                    obj[delivery.player_dismissed] = {};
                    obj[delivery.player_dismissed][delivery.bowler] = 1;
                }
                return obj;
            }, {});

            const batsMans = Object.keys(result);
            const highestTimesDismissal = batsMans.reduce((obj, key) => {
                const values = Object.entries(result[key]);
                values.sort((val1, val2) => {
                    return val2[1] - val1[1];
                });
                if (obj.dismissalTimes < values[0][1]) {
                    obj.dismissalTimes = values[0][1];
                    obj.batsMans = key;
                    obj.bowler = values[0][0];
                }
                return obj;
            }, { batsMans: "", bowler: "", dismissalTimes: 0 });

            res.status(200).json(highestTimesDismissal).end();
        })
        .catch(error => {
            console.error(error);
            next({
                status: 500,
                message: error.message
            });
        });
};


module.exports = one_player_dismissed_by_another_player;
