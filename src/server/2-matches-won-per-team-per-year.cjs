// ================================ problem 2 ========================



// Number of matches won per team per year in IPL.



//====================================================================

const path = require('path');

const csvtojson = require('csvtojson');

const matches_csv_file = path.join(__dirname, "../data/matches.csv");

const number_of_matches_won_per_team = function (req, res, next) {

    csvtojson()
        .fromFile(matches_csv_file)
        .then(jsonObj => {

            // const result = {} // eg : {2008 :{KKR:6,CSK:9,DD:7},};
            const result = jsonObj.reduce((obj, match) => {
                if (obj[match.season] == undefined) {
                    obj[match.season] = {};
                    obj[match.season][match.winner] = 1;
                } else {
                    if (obj[match.season][match.winner] == undefined) {
                        obj[match.season][match.winner] = 1;
                    } else {
                        obj[match.season][match.winner]++;
                    }
                }
                return obj;
            }, {});
            res.status(200).json(result).end();
        })
        .catch(error => {
            console.error(error);
            next({
                status: 500,
                message: error.message
            });
        });
};



module.exports = number_of_matches_won_per_team;