// ================================ problem 1 ========================



// Number of matches played per year for all the years in IPL.



//====================================================================

const path = require('path');

const csvtojson = require('csvtojson');

const matches_csv_file = path.join(__dirname, "../data/matches.csv");


const number_of_matches_played_per_year = function (req, res, next) {

    csvtojson()
        .fromFile(matches_csv_file)
        .then(jsonObj => {
            const result = jsonObj.reduce((obj, match) => {
                if (obj[match.season] == undefined) {
                    obj[match.season] = 1;
                } else {
                    obj[match.season]++;
                }
                return obj;
            }, {});
            res.status(200).json(result).end();
        })
        .catch(error => {
            console.error(error);
            next({
                status: 500,
                message: error.message
            });
        });
};

module.exports = number_of_matches_played_per_year;