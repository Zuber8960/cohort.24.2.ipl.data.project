const express = require("express");
const { PORT } = require("./config");
const path = require("path");
const requestId = require("express-request-id");

const app = express();

const router = require("./routes/index");
const middleware = require("./middleware/index");

app.use(requestId());
app.use(express.static(path.join(__dirname, "src/public/views")));

app.use(middleware.logHandlerMiddleware);
app.get("/log", middleware.logMiddleware);

app.use("/ipl", router);

app.use((req, res, next) => {
    next({
        status: 404,
        message: "Invalid Endpoint"
    })
});

app.use(middleware.errorHandler);

app.listen(PORT, () => {
    console.error("Server is live");
});