const express = require("express");
const router = express.Router();
const path = require("path");

const iplFilePath = path.join(__dirname, "../src/public/views/ipl.html");

const matchesPlayedPerYear = require("../src/server/1-matches-per-year.cjs");
const matchesWonPerTeamPerYear = require("../src/server/2-matches-won-per-team-per-year.cjs");
const extraRunsConceded = require("../src/server/3-extra-runs-conceded-per-team-in-the-year-2016.cjs");
const topTenEconomicalBowler = require("../src/server/4-top-10-economical-bowlers.cjs");
const wonTheTossWonTheMatch = require("../src/server/5-won-the-toss-and-also-won-the-match.cjs");
const highestPlayerOfTheMatch = require("../src/server/6-highest-number-of-Player-of-the-Match.cjs");
const strikeRateOfBatsman = require("../src/server/7-strike-of-a-batsman.cjs");
const highestDismissal = require("../src/server/8-highest-number-of-one-player-dismissed-by-another-player.cjs");
const bowlerWithBestEconomyInSuperOver = require("../src/server/9-bowler-with-the-best-economy-in-superover.cjs");

router.get("/" , (req, res, next) => {
    res.sendFile(iplFilePath);
});

router.get("/matchesPlayedPerYear", matchesPlayedPerYear);
router.get("/matchesWonPerTeamPerYear", matchesWonPerTeamPerYear);
router.get("/extraRunsConceded", extraRunsConceded);
router.get("/topTenEconomicalBowler", topTenEconomicalBowler);
router.get("/wonTheTossWonTheMatch", wonTheTossWonTheMatch);
router.get("/highestPlayerOfTheMatch", highestPlayerOfTheMatch);
router.get("/strikeRateOfBatsman", strikeRateOfBatsman);
router.get("/highestDismissal", highestDismissal);
router.get("/bowlerWithBestEconomyInSuperOver", bowlerWithBestEconomyInSuperOver);


module.exports = router;